package test.java;

import com.example.marketmaker.*;
import com.example.marketmaker.algo.*;
import com.example.marketmaker.marketmaker.ExternalPriceConnection;
import com.example.marketmaker.marketmaker.MarketMaker;
import com.example.marketmaker.marketmaker.MarketMakerImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class FunctionalJunitTest {

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private static TestReferencePriceSource referencePriceSource;
    private static MarketMakerImpl marketMaker;

    @BeforeAll
    public static void setup() throws InterruptedException {
        //These variable should be imported by xml and the value should be determined and adjusted based on the result of performance testing
        int numberOfWorkerThreads = 10;

        //Price Strategy can be different by client, stock, market etc. For simplicity, only keep one set of strategy set here
        List<PriceStrategy> priceStrategies = new ArrayList<>();
        priceStrategies.add(new SidePriceStrategy());
        priceStrategies.add(new StockPriceStrategy());
        priceStrategies.add(new TieringPriceStrategy());

        //Price formula: Buy Price = reference price + stock code number + qty + 0.5; Sell Price = reference price + stock code number + qty - 0.5


        QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculatorEngineImpl(priceStrategies);

        List<Integer> stockList = new ArrayList<Integer>(100_000);
        for (int i = 1; i <= 90_000; i++) {
            stockList.add(i);
        }

        ExternalPriceConnection externalPriceConnection = new ExternalPriceConnection() {

            @Override
            public void subscribe(int stock, ReferencePriceSourceListener referencePriceSourceListener) {
                //do nothing
            }
        };

        referencePriceSource = new TestReferencePriceSource(externalPriceConnection);
        marketMaker = new MarketMakerImpl(quoteCalculationEngine, referencePriceSource, stockList);
        marketMaker.start();
        AioServerHandler aioServerHandler = new AioServerHandler(numberOfWorkerThreads,marketMaker);

        AioSocketServer server = new AioSocketServer(aioServerHandler);
        server.start(8888);

        Thread.sleep(3000);
    }


    //Price formula: Buy Price = reference price + stock code number + qty + 0.5; Sell Price = reference price + stock code number + qty - 0.5
    private static Stream<Arguments> data() {

        return Stream.of(
                //No market data
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, "123 BUY 100", "-1"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "91111 BUY 100", "-1"),
                //test different form of invalid request
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "123 BUY", "-1"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, " ", "-1"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "134 SELLL 100", "-1"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "-134 BUY 100", "-1"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "BUY 100", "-1"),
                //regular positive test case - response changed for stock
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "123 BUY 100", "323.5"),
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "32 BUY 100", "182.5"),
                //regular positive test case - response changed for qty
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "32 BUY 1000", "1082.5"),
                //regular positive test case - response changed for side
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 50d);
                    }
                }, "32 SELL 1000", "1081.5"),
                //regular positive test case - response changed for reference price
                Arguments.arguments(new Runnable() {
                    @Override
                    public void run() {
                        referencePriceSource.referencePriceChanged(1, 1d);
                        referencePriceSource.referencePriceChanged(123, 100d);
                        referencePriceSource.referencePriceChanged(32, 100d);
                    }
                }, "32 SELL 1000", "1131.5")
                );
    }


    @ParameterizedTest
    @MethodSource("data")
    public void testResponseWithNoMarketData(Runnable priceUpdate, String request, String expectedResult) throws Exception {

        marketMaker.cleanupPrice();

        priceUpdate.run();

        Thread.sleep(50);

        AioSocketClient myClient = new AioSocketClient();
        myClient.connect();

        executorService.submit(myClient);
        //new Thread(myClient, "myClient").start();
        String response = myClient.write(request);

        Assertions.assertEquals(expectedResult, response);
        myClient.stop();

    }

    @AfterAll
    public static void cleanup() {

    }

}
