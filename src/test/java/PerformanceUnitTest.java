package test.java;

import com.example.marketmaker.*;
import com.example.marketmaker.algo.*;
import com.example.marketmaker.marketmaker.ExternalPriceConnection;
import com.example.marketmaker.marketmaker.MarketMakerImpl;
import com.example.marketmaker.marketmaker.TestExternalPriceConnection;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class PerformanceUnitTest {

    private static TestReferencePriceSource referencePriceSource;
    private static MarketMakerImpl marketMaker;

    @BeforeAll
    public static void setup() throws InterruptedException {
        //These variable should be imported by xml and the value should be determined and adjusted based on the result of performance testing
        int numberOfWorkerThreads = 10;

        //Price Strategy can be different by client, stock, market etc. For simplicity, only keep one set of strategy set here
        List<PriceStrategy> priceStrategies = new ArrayList<>();
        priceStrategies.add(new SidePriceStrategy());
        priceStrategies.add(new StockPriceStrategy());
        priceStrategies.add(new TieringPriceStrategy());
        priceStrategies.add(new SlowPriceStrategy());

        QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculatorEngineImpl(priceStrategies);

        List<Integer> stockList = new ArrayList<Integer>(100_000);
        for (int i = 1; i <= 100; i++) {
            stockList.add(i);
        }

        TestExternalPriceConnection externalPriceConnection = new TestExternalPriceConnection(500);
        externalPriceConnection.start();

        referencePriceSource = new TestReferencePriceSource(externalPriceConnection);
        marketMaker = new MarketMakerImpl(quoteCalculationEngine, referencePriceSource, stockList);
        marketMaker.start();
        AioServerHandler aioServerHandler = new AioServerHandler(numberOfWorkerThreads,marketMaker);

        AioSocketServer server = new AioSocketServer(aioServerHandler);
        server.start(8888);

        Thread.sleep(5000);
    }


    @Test
    //The minimum price calculation duration is 100 millisecond due to SlowPriceStrategy sleeping for 100 milliseconds.
    //TestExternalPriceConnection will refresh the price of 100 stocks every 0.5 seconds
    //Acceptance Condition: 1000 requests are sent from 5 threads and all of them finish within 200 milliseconds
    public void testResponseWithNoMarketData() throws Exception {

        ExecutorService executorService = Executors.newFixedThreadPool(5);

        List<Future<Long>> futures = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {

            Callable<Long> callableTask = () -> {
                AioSocketClient myClient = new AioSocketClient();
                myClient.connect();
                new Thread(myClient, "myClient").start();

                Thread.sleep(5);

                long start = System.currentTimeMillis();
                myClient.write("10 BUY 100");
                long end = System.currentTimeMillis();
                myClient.stop();

                return end - start;
            };

            Future<Long> future = executorService.submit(callableTask);
            futures.add(future);
        }


        for (Future<Long> future: futures) {
            Long duration = future.get();
            Assertions.assertTrue(duration < 200);
        }


    }


}
