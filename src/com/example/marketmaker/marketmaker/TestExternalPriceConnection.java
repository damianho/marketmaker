package com.example.marketmaker.marketmaker;

import com.example.marketmaker.ReferencePriceSourceListener;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestExternalPriceConnection implements ExternalPriceConnection {

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private final ConcurrentHashMap<Integer, Set<ReferencePriceSourceListener>> listenersByStock;
    private final int milliseoondToRefresh;

    public TestExternalPriceConnection(int milliseoondToRefresh) {
        this.milliseoondToRefresh = milliseoondToRefresh;
        this.listenersByStock = new ConcurrentHashMap<Integer, Set<ReferencePriceSourceListener>>();
    }

    public void start() {
        executorService.execute(new Runnable() {

            @Override
            public void run() {

                while (true) {

                    Set<Map.Entry<Integer, Set<ReferencePriceSourceListener>>> entries = listenersByStock.entrySet();

                    for (Map.Entry<Integer, Set<ReferencePriceSourceListener>> entry : entries) {

                        Integer securityId = entry.getKey();
                        Random random = new Random();

                        double price = securityId + random.nextDouble();

                        for (ReferencePriceSourceListener listener : entry.getValue()) {
                            //System.out.println("Simulator send security " + securityId + " price " + price);
                            listener.referencePriceChanged(securityId, price);
                        }
                    }


                    try {
                        Thread.sleep(milliseoondToRefresh);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void subscribe(int stock, ReferencePriceSourceListener referencePriceSourceListener) {

        Set<ReferencePriceSourceListener> referencePriceSourceListeners = listenersByStock.get(stock);

        if (referencePriceSourceListeners == null) {

            Set<ReferencePriceSourceListener> newListeners = Collections.newSetFromMap(new ConcurrentHashMap<ReferencePriceSourceListener, Boolean>());
            Set<ReferencePriceSourceListener> existingListeners = listenersByStock.putIfAbsent(stock, newListeners);

            if (existingListeners != null) {
                existingListeners.add(referencePriceSourceListener);
            }
            else {
                newListeners.add(referencePriceSourceListener);
            }

        }
        else {
            referencePriceSourceListeners.add(referencePriceSourceListener);
        }

    }



}
