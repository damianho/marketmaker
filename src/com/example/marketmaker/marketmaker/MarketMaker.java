package com.example.marketmaker.marketmaker;

import com.example.marketmaker.quote.QuoteRequest;

public interface MarketMaker {

    double handleQuoteRequest(QuoteRequest quoteRequest);
}
