package com.example.marketmaker.marketmaker;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.quote.QuoteRequest;

import java.util.ArrayList;
import java.util.List;

public class MarketMakerImpl implements MarketMaker {

    private final QuoteCalculationEngine quoteCalculationEngine;
    private final ReferencePriceSource referencePriceSource;

    private final PriceHolder[] priceHolders = new PriceHolder[100_000];
    private final List<Integer> stockList;

    public MarketMakerImpl(QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource, List<Integer> stockList) {
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
        this.stockList = stockList;
    }

    public void start() {
        subscribeAll(stockList);
    }

    private void subscribeAll(List<Integer> stockList) {

        for (int securityId : stockList) {
            priceHolders[securityId] = new PriceHolder(securityId);
        }

        this.referencePriceSource.subscribe((securityId, price) -> {

            if (priceHolders[securityId] == null) {
                System.out.println("Error: receive a price of a stock not subscribed");
            }
            else {
                priceHolders[securityId].updateReferencePrice(price);
            }
        });

        this.referencePriceSource.sendPriceRequest(stockList);
    }

    @Override
    public double handleQuoteRequest(QuoteRequest quoteRequest) {

        int securityId = quoteRequest.getSecurityId();
        int qty = quoteRequest.getQty();
        boolean buy = quoteRequest.isBuy();

            //security id is not in the supported stock list defined so return -1 price
        PriceHolder priceHolder = priceHolders[securityId];
        if (priceHolder == null) {
            return -1;
        }
        else {
            double referencePrice = priceHolder.getReferencePrice();

            if (referencePrice <= 0) {
                return -1;
            }

            double price = quoteCalculationEngine.calculateQuotePrice(securityId, referencePrice, buy, qty);
            return price;
        }
    }

    //For testing only
    public void cleanupPrice() {
        for (PriceHolder priceHolder : priceHolders) {
            if (priceHolder != null) {
                priceHolder.updateReferencePrice(-1);
            }
        }
    }
}
