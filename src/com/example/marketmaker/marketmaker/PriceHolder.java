package com.example.marketmaker.marketmaker;

public class PriceHolder {

    private final int securityId;
    private volatile double referencePrice = -1;

    public PriceHolder(int securityId) {
        this.securityId = securityId;
    }

    public void updateReferencePrice(double referencePrice) {
        this.referencePrice = referencePrice;
    }

    public double getReferencePrice() {
        return this.referencePrice;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PriceHolder{");
        sb.append("securityId=").append(securityId);
        sb.append(", referencePrice=").append(referencePrice);
        sb.append('}');
        return sb.toString();
    }
}
