package com.example.marketmaker.marketmaker;

import com.example.marketmaker.ReferencePriceSourceListener;

public interface ExternalPriceConnection {

    void subscribe(int stock, ReferencePriceSourceListener referencePriceSourceListener);

}
