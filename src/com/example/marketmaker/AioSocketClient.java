package com.example.marketmaker;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.atomic.AtomicBoolean;

public class AioSocketClient implements Runnable {

    private static Integer PORT = 8888;
    private static String IP_ADDRESS = "127.0.0.1";
    private AsynchronousSocketChannel asynSocketChannel ;

    private AtomicBoolean isStop = new AtomicBoolean(false);

    public AioSocketClient() throws Exception {
        asynSocketChannel = AsynchronousSocketChannel.open();  // Open the channel
    }
    public void connect(){
        asynSocketChannel.connect(new InetSocketAddress(IP_ADDRESS, PORT));  // Creating connections is the same as NIO
    }

    //return the response
    public String write(String request) throws Exception{

        try {
            asynSocketChannel.write(ByteBuffer.wrap(request.getBytes())).get();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            asynSocketChannel.read(byteBuffer).get();
            byteBuffer.flip();

            byte[] respByte = new byte[byteBuffer.remaining()];
            byteBuffer.get(respByte); // Put buffer data into byte arrays
            String response = new String(respByte, "utf-8").trim();
            System.out.println("Received Response:" + response);
            return response;

        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new Exception("No Response");
    }
    @Override
    public void run() {
        while(!isStop.get()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        isStop.set(true);
    }

}
