package com.example.marketmaker;

import com.example.marketmaker.marketmaker.MarketMaker;
import com.example.marketmaker.quote.QuoteRequest;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AioServerHandler implements CompletionHandler<AsynchronousSocketChannel, AioSocketServer> {

    //max request size = 2 * length of max integer in string representation * 2 + space length * 2 + side length = 10 * 2 + 2 + 4 = 52
    private final Integer BUFFER_SIZE = 1024;

    private final ExecutorService workers;
    private final MarketMaker marketMaker;

    public AioServerHandler(int numberOfWorkerThreads, MarketMaker marketMaker) {
        this.workers = Executors.newFixedThreadPool(numberOfWorkerThreads);
        this.marketMaker = marketMaker;
    }

    @Override
    public void completed(AsynchronousSocketChannel asynSocketChannel, AioSocketServer attachment) {
        // Ensure that multiple clients can block
        attachment.asynServerSocketChannel.accept(attachment, this);
        read(asynSocketChannel);
    }
    //Read data
    private void read(final AsynchronousSocketChannel asynSocketChannel) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        asynSocketChannel.read(byteBuffer, byteBuffer, new CompletionHandler<Integer, ByteBuffer>() {
            @Override
            public void completed(Integer resultSize, ByteBuffer attachment) {
                //After reading, reset the identifier bit
                attachment.flip();
                //Get the number of bytes read
                System.out.println("Server -> " + "The length of data received from the client is:" + resultSize);
                //Get the read data
                attachment.array();
                String resultData = new String(attachment.array()).trim();
                System.out.println("Server -> " + "The data information received from the client is:" + resultData);

                StringTokenizer stringTokenizer = new StringTokenizer(resultData, " ");

                int countTokens = stringTokenizer.countTokens();

                QuoteRequest quoteRequest = createQuoteRequest(resultData, asynSocketChannel);

                if (quoteRequest != null) {
                    workers.execute(() -> {

                        double price = marketMaker.handleQuoteRequest(quoteRequest);

                        if (price > 0) {
                            write(asynSocketChannel, price + "");
                        }
                        else {
                            giveErrorResponse(asynSocketChannel);
                        }

                    });
                }
                else {
                    giveErrorResponse(asynSocketChannel);
                }

                //String response = "Server response, Received data from client: " + resultData;
                //write(asynSocketChannel, response);
            }
            @Override
            public void failed(Throwable exc, ByteBuffer attachment) {
                exc.printStackTrace();
            }
        });
    }

    private QuoteRequest createQuoteRequest(String requestString, AsynchronousSocketChannel asynSocketChannel) {

        QuoteRequest quoteRequest;

        StringTokenizer stringTokenizer = new StringTokenizer(requestString, " ");

        int countTokens = stringTokenizer.countTokens();

        if (countTokens == 3) {
            String stockStr = stringTokenizer.nextToken();

            try {
                int stock = -1;
                stock = Integer.parseInt(stockStr);

                String side = stringTokenizer.nextToken();

                boolean isBuy;
                if ("BUY".equals(side)) {
                    isBuy = true;
                }
                else if ("SELL".equals(side)) {
                    isBuy = false;
                }
                else {
                    System.out.println("Error on handling the request: " + requestString + ", asynSocketChannel: " + asynSocketChannel);
                    return null;
                }

                String qtyStr = stringTokenizer.nextToken();
                int qty = Integer.parseInt(qtyStr);

                if (stock > 0 && qty > 0) {
                    quoteRequest = new QuoteRequest(asynSocketChannel, stock, isBuy, qty);
                    return quoteRequest;
                }
                else {
                    System.out.println("Error on handling the request: " + requestString + ", asynSocketChannel: " + asynSocketChannel);
                    return null;
                }

            }
            catch (Exception ex) {
                System.out.println("Error on handling the request: " + requestString + ", asynSocketChannel: " + asynSocketChannel);
                return null;
            }
        }
        else {
            System.out.println("Error on handling the request: " + requestString + ", asynSocketChannel: " + asynSocketChannel);
            return null;
        }
    }

    private void giveErrorResponse(AsynchronousSocketChannel asynSocketChannel) {
        write(asynSocketChannel, "-1");
    }

    // Write data
    private void write(AsynchronousSocketChannel asynSocketChannel, String response) {
        try {
            // Write the data into the buffer
            ByteBuffer buf = ByteBuffer.allocate(BUFFER_SIZE);
            buf.put(response.getBytes());
            buf.flip();
            // Write from buffer to channel
            asynSocketChannel.write(buf).get();
            System.out.println("Server Response:" + response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void failed(Throwable exc, AioSocketServer attachment) {
        exc.printStackTrace();
    }

}
