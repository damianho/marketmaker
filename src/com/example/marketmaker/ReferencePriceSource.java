package com.example.marketmaker;

import java.util.List;

/**
 * Source for reference prices.
 */
public interface ReferencePriceSource {
    /**
     * Subscribe to changes to refernce prices.
     *
     * @param listener callback interface for changes
     */
    void subscribe(ReferencePriceSourceListener listener);

    void sendPriceRequest(List<Integer> securityIdListToSubscribe);

    @Deprecated
    //It could become a blocking call if price source make a subscription and wait the price response from external source to return a price.
    //Since server is designed to be capable of handling a large number of quote requests and be able to respond in a timely manner
    //We don't want to call it.
    double get(int securityId);
}
