package com.example.marketmaker;

import com.example.marketmaker.marketmaker.ExternalPriceConnection;
import com.example.marketmaker.marketmaker.PriceHolder;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestReferencePriceSource implements ReferencePriceSource, ReferencePriceSourceListener {

    private List<ReferencePriceSourceListener> referencePriceSourceListeners = new ArrayList<>();

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    //Price source does not need to have price cache if it provide api to subscribe the price update
    private PriceHolder[] priceHolders = new PriceHolder[100_000];

    private final ExternalPriceConnection externalPriceConnection;


    public TestReferencePriceSource(ExternalPriceConnection externalPriceConnection) {
        this.externalPriceConnection = externalPriceConnection;
    }

    public void sendPriceRequest(List<Integer> securityIdListToSubscribe) {
        for (Integer securityId : securityIdListToSubscribe) {
            externalPriceConnection.subscribe(securityId, this);
        }
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        executorService.execute(() -> {
            referencePriceSourceListeners.add(listener);
        });
    }

    @Override
    public double get(int securityId) {
        throw new RuntimeException("Not Implemented");
    }

    @Override
    public void referencePriceChanged(int securityId, double price) {
        executorService.execute(() -> {

            for (ReferencePriceSourceListener referencePriceSourceListener : referencePriceSourceListeners) {
                referencePriceSourceListener.referencePriceChanged(securityId, price);
            }
        });
    }

}
