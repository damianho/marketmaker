package com.example.marketmaker.quote;

import java.nio.channels.AsynchronousSocketChannel;

public class QuoteRequest {

    private final AsynchronousSocketChannel asynchronousSocketChannel;
    private final int securityId;
    private final boolean isBuy;
    private final int qty;

    public QuoteRequest(AsynchronousSocketChannel asynchronousSocketChannel, int securityId, boolean isBuy, int qty) {
        this.asynchronousSocketChannel = asynchronousSocketChannel;
        this.securityId = securityId;
        this.isBuy = isBuy;
        this.qty = qty;
    }

    public AsynchronousSocketChannel getAsynchronousSocketChannel() {
        return asynchronousSocketChannel;
    }

    public int getSecurityId() {
        return securityId;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public int getQty() {
        return qty;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QuoteRequest{");
        sb.append("asynchronousSocketChannel=").append(asynchronousSocketChannel);
        sb.append(", securityId=").append(securityId);
        sb.append(", isBuy=").append(isBuy);
        sb.append(", qty=").append(qty);
        sb.append('}');
        return sb.toString();
    }

}
