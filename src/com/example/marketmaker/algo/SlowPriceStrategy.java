package com.example.marketmaker.algo;

public class SlowPriceStrategy implements PriceStrategy {


    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return referencePrice;
    }
}
