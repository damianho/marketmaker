package com.example.marketmaker.algo;

public interface PriceStrategy {

    double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity);

}
