package com.example.marketmaker.algo;

public class TieringPriceStrategy implements PriceStrategy {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {

        double referencePriceCalculated =  referencePrice + Double.valueOf(quantity);

        return referencePriceCalculated;
    }
}
