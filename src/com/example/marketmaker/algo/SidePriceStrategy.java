package com.example.marketmaker.algo;

public class SidePriceStrategy implements PriceStrategy {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {

        referencePrice = buy? referencePrice + 0.5d: referencePrice - 0.5d;

        if (referencePrice <= 0) {
            referencePrice = 0.1d;
        }

        return referencePrice;
    }
}
