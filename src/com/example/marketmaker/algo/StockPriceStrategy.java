package com.example.marketmaker.algo;

public class StockPriceStrategy implements PriceStrategy {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {

        referencePrice += securityId;

        return referencePrice;
    }
}
