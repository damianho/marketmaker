package com.example.marketmaker;

import com.example.marketmaker.algo.*;
import com.example.marketmaker.marketmaker.ExternalPriceConnection;
import com.example.marketmaker.marketmaker.MarketMaker;
import com.example.marketmaker.marketmaker.MarketMakerImpl;
import com.example.marketmaker.marketmaker.TestExternalPriceConnection;

import java.util.ArrayList;
import java.util.List;

public class Main {

    //Assumption:
    //1. It should be one request one response because the API of un-subscription is not defined.
    //2. Server respond -1 to client for any type of errors or no market data
    //3. System.out.println can be replaced by other logging framework and the concatenation of logging string can be further improved to reduce object creation. But for simplicity,  I still use System.out.println.
    //4. Server is designed to subscribe the market data of all stocks at start up so the price held in memory can be assumed to be latest price.
    //    So the call to MarketMaker.handleQuoteRequest can be assumed to non-blocking
    //5. Since stock code is Integer, I made an assumption that the range of stock code is from 1 to 90000 so I can use an array to store the stock reference price instead of concurrent hash map to further reduce lock contention.
    //    For example, the range of stock code in HK exchange is from 1 - 89999
    //    https://www.hkex.com.hk/Products/Securities/Equities/Naming-Convetions-of-Stock-Short-Name-and-Stock-Code-Allocation-Plan-for-Stapled-Securities?sc_lang=en
    //6. For monetary calculation, we should never use double or float since it can cause loss of precision. We should use BigDecimal or use long for storing cents and calculation or still use double but round the result when necessary.
    //7. ReferencePriceSource.get(int securityId) will not be implemented because it can become a blocking call if price source make a subscription and wait the price response from external source to return a price.
    //   Since server is designed to be capable of handling a large number of quote requests and be able to respond in a timely manner
    //   Blocking call should be avoided
    //8. Create a simple function junit test which can be easily added with different test cases
    //9. Create a basic performance test to simulate server receive a burst of quote request and can respond in timely manner

    //These variable can be imported by spring configuration
    private static final int MILLI_SECOND_TO_REFRESH_PRICE = 100;
    private static final ExternalPriceConnection externalPriceConnection = new TestExternalPriceConnection(1000);

    public static void main(String[] args) {

        //These variable should be imported by xml and the value should be determined and adjusted based on the result of performance testing
        int numberOfWorkerThreads = 10;

        //Price Strategy can be different by client, stock, market etc. For simplicity, only keep one set of strategy set here
        List<PriceStrategy> priceStrategies = new ArrayList<>();
        priceStrategies.add(new SidePriceStrategy());
        priceStrategies.add(new SlowPriceStrategy());
        priceStrategies.add(new StockPriceStrategy());
        priceStrategies.add(new TieringPriceStrategy());

        QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculatorEngineImpl(priceStrategies);

        List<Integer> stockList = new ArrayList<Integer>(100_000);
        for (int i = 1; i <= 90_000; i++) {
            stockList.add(i);
        }

        ReferencePriceSource referencePriceSource = new TestReferencePriceSource(externalPriceConnection);
        MarketMaker marketMaker = new MarketMakerImpl(quoteCalculationEngine, referencePriceSource, stockList);
        AioServerHandler aioServerHandler = new AioServerHandler(numberOfWorkerThreads,marketMaker);

        AioSocketServer server = new AioSocketServer(aioServerHandler);
        server.start(8888);
    }

}
