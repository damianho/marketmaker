package com.example.marketmaker;

import com.example.marketmaker.algo.PriceStrategy;

import java.util.List;

public class QuoteCalculatorEngineImpl implements QuoteCalculationEngine {

    private final List<PriceStrategy> priceStrategies;

    public QuoteCalculatorEngineImpl(List<PriceStrategy> priceStrategies) {
        this.priceStrategies = priceStrategies;
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {

        double resolvedReferencePrice = referencePrice;

        for (PriceStrategy priceStrategy:  priceStrategies) {
            resolvedReferencePrice = priceStrategy.calculateQuotePrice(securityId, resolvedReferencePrice, buy, quantity);
        }

        return resolvedReferencePrice;
    }
}
