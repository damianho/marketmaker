package com.example.marketmaker;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AioSocketServer {

    private ExecutorService executorService;          // Thread pool
    private AsynchronousChannelGroup threadGroup;      // Channel group
    public AsynchronousServerSocketChannel asynServerSocketChannel;  // Server channel

    private final AioServerHandler aioServerHandler;

    public AioSocketServer(AioServerHandler aioServerHandler) {
        this.aioServerHandler = aioServerHandler;
    }

    public void start(Integer port){
        try {
            // 1. Create a cache pool
            executorService = Executors.newCachedThreadPool();
            // 2. Creating Channel Groups
            threadGroup = AsynchronousChannelGroup.withCachedThreadPool(executorService, 1);
            // 3. Create server channels
            asynServerSocketChannel = AsynchronousServerSocketChannel.open(threadGroup);
            // 4. Binding
            asynServerSocketChannel.bind(new InetSocketAddress(port));
            System.out.println("server start , port : " + port);
            // 5. Waiting for Client Request
            asynServerSocketChannel.accept(this, aioServerHandler);
            // The server is blocked all the time, and the real environment is running under tomcat, so this line of code is not needed.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
